package net.therap.controller;

import net.therap.service.database.ConnectionService;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class DatabaseController {

    private static Connection connection;

    public static void initialize(String dbname, String username, String password) {
        connection = ConnectionService.getConnection(dbname, username, password);
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
