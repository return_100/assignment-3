package net.therap.controller;

import net.therap.service.home.HomeService;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class HomeController {

    public static String getRole(HomeService homeService) {
        return homeService.getRole();
    }
}
