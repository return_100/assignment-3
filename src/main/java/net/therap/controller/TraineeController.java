package net.therap.controller;

import net.therap.service.trainee.TraineeService;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class TraineeController {

    public static void getService(TraineeService traineeService) {
        traineeService.process();
    }
}
