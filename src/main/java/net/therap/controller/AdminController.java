package net.therap.controller;

import net.therap.service.admin.AdminService;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class AdminController {

    public static void getService(AdminService adminService) {
        adminService.process();
    }
}
