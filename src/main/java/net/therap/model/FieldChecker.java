package net.therap.model;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public interface FieldChecker<T> {

    boolean check(T object, FieldAttribute attributes);
}
