package net.therap.model;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class FieldAttribute {

    private int min;
    private int max;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
