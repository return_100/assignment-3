package net.therap.model;

import net.therap.service.validator.EmailValidator;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class Trainee {

    private int id;
    private String name;
    @FieldCheck(checker = EmailValidator.class)
    private String email;
    private String password;

    public Trainee() {
    }

    public Trainee(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
