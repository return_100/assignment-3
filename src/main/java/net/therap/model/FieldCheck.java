package net.therap.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author al.imran
 * @since 01/04/2021
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FieldCheck {

    int min() default 1;

    int max() default 10;

    Class<? extends FieldChecker> checker();
}
