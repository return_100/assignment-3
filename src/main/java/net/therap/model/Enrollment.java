package net.therap.model;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class Enrollment {

    private Course course;
    private Trainee trainee;

    public Enrollment(Course course, Trainee trainee) {
        this.course = course;
        this.trainee = trainee;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }
}
