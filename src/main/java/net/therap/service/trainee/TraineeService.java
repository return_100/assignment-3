package net.therap.service.trainee;

import net.therap.dao.CourseDao;
import net.therap.dao.EnrollmentDao;
import net.therap.dao.TraineeDao;
import net.therap.model.Course;
import net.therap.model.Enrollment;
import net.therap.model.FieldCheck;
import net.therap.model.Trainee;
import net.therap.service.validator.RangeValidator;
import net.therap.service.validator.Validate;
import net.therap.view.MessageView;
import net.therap.view.Messages;

import java.util.List;
import java.util.Scanner;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class TraineeService {

    @FieldCheck(max = 7, checker = RangeValidator.class)
    private int option;
    private int traineeId;

    private final TraineeDao traineeDao;
    private final CourseDao courseDao;
    private final EnrollmentDao enrollmentDao;

    private static final int INVALID_TRAINEE_ID = 0;
    private static final int VALID_TRAINEE_LIST_SIZE = 1;
    private static final int REGISTER = 1;
    private static final int LOGIN = 2;
    private static final int SHOW_ALL_COURSE = 3;
    private static final int SHOW_ENROLLED_COURSE = 4;
    private static final int ENROLLED_IN_COURSE = 5;
    private static final int UNENROLLED_FROM_COURSE = 6;
    private static final int EXIT = 7;
    private static final String OPTION = "option";
    private static final String EMAIL = "email";

    public TraineeService() {
        this.traineeDao = new TraineeDao();
        this.courseDao = new CourseDao();
        this.enrollmentDao = new EnrollmentDao();
    }

    public void process() {
        boolean isLoggedIn = false;

        while (true) {

            MessageView.show(Messages.TRAINEE_OPTIONS);
            Scanner scn = new Scanner(System.in);
            option = scn.nextInt();

            if (Validate.check(this, OPTION)) {
                if (option == REGISTER) {
                    MessageView.show(Messages.ENTER_NAME);
                    String name = scn.next();

                    MessageView.show(Messages.ENTER_EMAIL);
                    String email = scn.next();

                    MessageView.show(Messages.ENTER_PASSWORD);
                    String password = scn.next();

                    if (register(name, email, password)) {
                        MessageView.show(Messages.SUCCESSFUL_REGISTRATION);
                    } else {
                        MessageView.show(Messages.UNSUCCESSFUL_REGISTRATION);
                    }

                    isLoggedIn = false;
                }

                if (option == LOGIN) {
                    MessageView.show(Messages.ENTER_EMAIL);
                    String email = scn.next();

                    MessageView.show(Messages.ENTER_PASSWORD);
                    String password = scn.next();
                    traineeId = login(email, password);

                    if (traineeId != INVALID_TRAINEE_ID) {
                        isLoggedIn = true;
                        MessageView.show(Messages.SUCCESSFUL_LOGIN);
                    } else {
                        isLoggedIn = false;
                        MessageView.show(Messages.UNSUCCESSFUL_LOGIN);
                    }
                }

                if (option == SHOW_ALL_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ALL_COURSE_LIST);
                    List<Course> courses = courseDao.findAll();

                    for (Course course : courses) {
                        MessageView.show(course.getId() + " " + course.getName());
                    }
                }

                if (option == SHOW_ENROLLED_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ENROLLED_COURSE_LIST);
                    List<Course> courses = enrollmentDao.find(traineeId);

                    for (Course course : courses) {
                        MessageView.show(course.getId() + " " + course.getName());
                    }
                }

                if (option == ENROLLED_IN_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ENTER_COURSE_ID);
                    int courseId = scn.nextInt();

                    Course course = new Course(courseId);
                    Trainee trainee = new Trainee(traineeId);
                    enrollmentDao.save(new Enrollment(course, trainee));
                }

                if (option == UNENROLLED_FROM_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ENTER_COURSE_ID);
                    int courseId = scn.nextInt();

                    Course course = new Course(courseId);
                    Trainee trainee = new Trainee(traineeId);
                    enrollmentDao.delete(new Enrollment(course, trainee));
                }

                if (option == EXIT) {
                    MessageView.show(Messages.EXIT);
                    break;
                }
            }
        }
    }

    private boolean register(String name, String email, String password) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        trainee.setName(name);
        trainee.setPassword(password);

        if (Validate.check(trainee, EMAIL)) {
            return traineeDao.save(trainee);
        } else {
            return false;
        }
    }

    private int login(String email, String password) {
        Trainee trainee = new Trainee();
        trainee.setEmail(email);
        trainee.setPassword(password);
        List<Trainee> traineeList = traineeDao.find(trainee);

        if (traineeList.size() == VALID_TRAINEE_LIST_SIZE) {
            return traineeList.get(0).getId();
        } else {
            return INVALID_TRAINEE_ID;
        }
    }
}
