package net.therap.service.validator;

import net.therap.model.FieldAttribute;
import net.therap.model.FieldChecker;

import java.util.regex.Pattern;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class EmailValidator implements FieldChecker<String> {

    private static final String EMAIL_REGEX = "[_a-zA-Z1-9]+(\\.[A-Za-z0-9]*)*@[A-Za-z0-9]+" +
            "\\.[A-Za-z0-9]+(\\.[A-Za-z0-9]*)*";

    @Override
    public boolean check(String object, FieldAttribute attributes) {
        return Pattern.matches(EMAIL_REGEX, object);
    }
}
