package net.therap.service.validator;

import net.therap.model.FieldAttribute;
import net.therap.model.FieldCheck;
import net.therap.model.FieldChecker;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class Validate {

    public static boolean check(Object object, String fieldname) {
        Field field = null;

        try {
            field = object.getClass().getDeclaredField(fieldname);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        if (field.isAnnotationPresent(FieldCheck.class)) {
            field.setAccessible(true);
            FieldCheck annotation = field.getAnnotation(FieldCheck.class);
            FieldAttribute fieldAttribute = new FieldAttribute();
            fieldAttribute.setMin(annotation.min());
            fieldAttribute.setMax(annotation.max());
            FieldChecker fieldChecker = null;

            try {
                fieldChecker = annotation.checker().getConstructor().newInstance();
                return fieldChecker.check(field.get(object), fieldAttribute);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
