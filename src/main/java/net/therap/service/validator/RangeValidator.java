package net.therap.service.validator;

import net.therap.model.FieldAttribute;
import net.therap.model.FieldChecker;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class RangeValidator implements FieldChecker<Integer> {

    @Override
    public boolean check(Integer object, FieldAttribute attributes) {
        int value = object;
        return (value >= attributes.getMin() && value <= attributes.getMax());
    }
}
