package net.therap.service.home;

import net.therap.model.FieldCheck;
import net.therap.service.validator.RangeValidator;
import net.therap.service.validator.Validate;
import net.therap.view.MessageView;
import net.therap.view.Messages;

import java.util.Scanner;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class HomeService {

    @FieldCheck(max = 3, checker = RangeValidator.class)
    private static Integer role;

    private static final int ADMIN_ROLE = 1;
    private static final int TRAINEE_ROLE = 2;
    private static final String ROLE = "role";
    private static final String ADMIN = "Admin";
    private static final String TRAINEE = "Trainee";
    private static final String EXIT = "Exit";

    public String getRole() {
        while (true) {

            MessageView.show(Messages.HOME_OPTIONS);
            Scanner scn = new Scanner(System.in);
            role = scn.nextInt();

            if (Validate.check(this, ROLE)) {
                if (role == ADMIN_ROLE) {
                    return ADMIN;
                } else if (role == TRAINEE_ROLE) {
                    return TRAINEE;
                } else {
                    return EXIT;
                }
            }
        }
    }
}
