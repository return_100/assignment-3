package net.therap.service.database;

import net.therap.view.MessageView;
import net.therap.view.Messages;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class ConnectionService {

    public static final String DB_URL = "jdbc:mysql://localhost:3306/";

    public static Connection getConnection(String dbName, String username, String password) {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DB_URL + dbName, username, password);

            if (Objects.nonNull(connection)) {
                MessageView.show(Messages.SUCCESSFUL_CONNECTION);
            } else {
                MessageView.show(Messages.UNSUCCESSFUL_CONNECTION);
            }
        } catch (SQLException throwables) {
            MessageView.show(throwables.getMessage());
        }

        return connection;
    }
}
