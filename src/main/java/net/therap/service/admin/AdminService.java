package net.therap.service.admin;

import net.therap.dao.AdminDao;
import net.therap.dao.CourseDao;
import net.therap.model.Admin;
import net.therap.model.Course;
import net.therap.model.FieldCheck;
import net.therap.service.validator.RangeValidator;
import net.therap.service.validator.Validate;
import net.therap.view.MessageView;
import net.therap.view.Messages;

import java.util.List;
import java.util.Scanner;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class AdminService {

    @FieldCheck(max = 5, checker = RangeValidator.class)
    private int option;

    private final AdminDao adminDao;
    private final CourseDao courseDao;

    private static final int LOGIN = 1;
    private static final int SHOW_ALL_COURSE = 2;
    private static final int ADD_COURSE = 3;
    private static final int DELETE_COURSE = 4;
    private static final int EXIT = 5;
    private static final int VALID_ADMIN_LIST_SIZE = 1;
    private static final String OPTION = "option";

    public AdminService() {
        this.adminDao = new AdminDao();
        this.courseDao = new CourseDao();
    }

    public void process() {
        boolean isLoggedIn = false;

        while (true) {

            MessageView.show(Messages.ADMIN_OPTIONS);
            Scanner scn = new Scanner(System.in);
            option = scn.nextInt();

            if (Validate.check(this, OPTION)) {
                if (option == LOGIN) {
                    MessageView.show(Messages.ENTER_EMAIL);
                    String email = scn.next();

                    MessageView.show(Messages.ENTER_PASSWORD);
                    String password = scn.next();

                    if (login(email, password)) {
                        isLoggedIn = true;
                        MessageView.show(Messages.SUCCESSFUL_LOGIN);
                    } else {
                        isLoggedIn = false;
                        MessageView.show(Messages.UNSUCCESSFUL_LOGIN);
                    }
                }

                if (option == SHOW_ALL_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ALL_COURSE_LIST);
                    List<Course> courses = courseDao.findAll();

                    for (Course course : courses) {
                        MessageView.show(course.getId() + " " + course.getName());
                    }
                }

                if (option == ADD_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ENTER_COURSE_NAME);
                    String coursename = scn.next();

                    Course course = new Course(coursename);
                    courseDao.save(course);
                }

                if (option == DELETE_COURSE && isLoggedIn) {
                    MessageView.show(Messages.ENTER_COURSE_NAME);
                    String coursename = scn.next();

                    Course course = new Course(coursename);
                    courseDao.delete(course);
                }

                if (option == EXIT) {
                    MessageView.show(Messages.EXIT);
                    break;
                }
            }
        }
    }

    private boolean login(String email, String password) {
        Admin admin = new Admin();
        admin.setEmail(email);
        admin.setPassword(password);
        List<Admin> adminList = adminDao.find(admin);
        return adminList.size() == VALID_ADMIN_LIST_SIZE;
    }
}
