package net.therap;

import net.therap.controller.AdminController;
import net.therap.controller.DatabaseController;
import net.therap.controller.HomeController;
import net.therap.controller.TraineeController;
import net.therap.service.admin.AdminService;
import net.therap.service.home.HomeService;
import net.therap.service.trainee.TraineeService;
import net.therap.view.MessageView;
import net.therap.view.Messages;

/**
 * @author al.imran
 * @since 31/03/2021
 */
public class Main {

    private static final String ADMIN = "Admin";
    private static final String TRAINEE = "Trainee";
    private static final int DB_NAME_IDX = 0;
    private static final int DB_USERNAME_IDX = 1;
    private static final int DB_PASSWORD_IDX = 2;
    private static final int ARGS_MIN_LENGTH = 3;

    public static void main(String[] args) {
        if (args.length < ARGS_MIN_LENGTH) {
            MessageView.show(Messages.NOT_ENOUGH_ARGUMENTS);
        } else {
            DatabaseController.initialize(args[DB_NAME_IDX], args[DB_USERNAME_IDX], args[DB_PASSWORD_IDX]);

            while (true) {

                String role = HomeController.getRole(new HomeService());

                if (role.equals(TRAINEE)) {
                    TraineeController.getService(new TraineeService());
                } else if (role.equals(ADMIN)) {
                    AdminController.getService(new AdminService());
                } else {
                    MessageView.show(Messages.EXIT);
                    break;
                }
            }

            DatabaseController.closeConnection();
        }
    }
}
