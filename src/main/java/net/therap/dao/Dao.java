package net.therap.dao;

import java.util.List;

/**
 * @author al.imran
 * @since 03/04/2021
 */
public interface Dao<T> {

    default List find(Object object) {
        return null;
    }

    default List<T> findAll() {
        return null;
    }

    default boolean save(T t) {
        return false;
    }

    default void delete(T t) {
    }
}
