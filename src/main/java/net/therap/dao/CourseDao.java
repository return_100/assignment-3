package net.therap.dao;

import net.therap.controller.DatabaseController;
import net.therap.model.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author al.imran
 * @since 03/04/2021
 */
public class CourseDao implements Dao<Course> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String SQL_READ = "SELECT * FROM course;";
    private static final String SQL_INSERT = "INSERT INTO course (name) VALUES (?);";
    private static final String SQL_DELETE = "DELETE FROM course WHERE NAME=?;";

    @Override
    public List<Course> findAll() {
        List<Course> list = new ArrayList<>();
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_READ);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(new Course(resultSet.getInt(ID), resultSet.getString(NAME)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

    @Override
    public boolean save(Course course) {
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            connection.setAutoCommit(false);
            statement.setString(1, course.getName());
            statement.execute();
            connection.commit();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @Override
    public void delete(Course course) {
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            connection.setAutoCommit(false);
            statement.setString(1, course.getName());
            statement.execute();
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
