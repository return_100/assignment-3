package net.therap.dao;

import net.therap.controller.DatabaseController;
import net.therap.model.Admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al.imran
 * @since 03/04/2021
 */
public class AdminDao implements Dao<Admin> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    public static final String SQL_INSERT = "INSERT INTO admin (name, email, password) VALUES (?, ?, ?);";
    public static final String SQL_READ = "SELECT * FROM admin WHERE email=? AND password=?;";

    @Override
    public List<Admin> find(Object object) {
        Admin admin = (Admin) object;
        List<Admin> list = new ArrayList<>();
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_READ);
            statement.setString(1, admin.getEmail());
            statement.setString(2, admin.getPassword());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Admin newAdmin = new Admin();
                newAdmin.setId(resultSet.getInt(ID));
                newAdmin.setName(resultSet.getString(NAME));
                newAdmin.setEmail(resultSet.getString(EMAIL));
                newAdmin.setPassword(resultSet.getString(PASSWORD));
                list.add(newAdmin);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

    @Override
    public boolean save(Admin admin) {
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            connection.setAutoCommit(false);
            statement.setString(1, admin.getName());
            statement.setString(2, admin.getEmail());
            statement.setString(3, admin.getPassword());
            statement.execute();
            connection.commit();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();

            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
