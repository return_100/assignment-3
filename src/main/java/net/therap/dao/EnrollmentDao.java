package net.therap.dao;

import net.therap.controller.DatabaseController;
import net.therap.model.Course;
import net.therap.model.Enrollment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author al.imran
 * @since 03/04/2021
 */
public class EnrollmentDao implements Dao<Enrollment> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String SQL_READ = "SELECT * FROM course JOIN enrollment " +
            "ON (course.id=enrollment.courseId) WHERE enrollment.traineeId=?;";
    private static final String SQL_INSERT = "INSERT INTO enrollment (courseId, traineeId) VALUES (?, ?);";
    private static final String SQL_DELETE = "DELETE FROM enrollment WHERE courseId=? AND traineeId=?;";

    @Override
    public List<Course> find(Object object) {
        int traineeId = (int) object;
        List<Course> list = new ArrayList<>();
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_READ);
            statement.setInt(1, traineeId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(new Course(resultSet.getInt(ID), resultSet.getString(NAME)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

    @Override
    public boolean save(Enrollment enrollment) {
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            connection.setAutoCommit(false);
            statement.setInt(1, enrollment.getCourse().getId());
            statement.setInt(2, enrollment.getTrainee().getId());
            statement.execute();
            connection.commit();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @Override
    public void delete(Enrollment enrollment) {
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            connection.setAutoCommit(false);
            statement.setInt(1, enrollment.getCourse().getId());
            statement.setInt(2, enrollment.getTrainee().getId());
            statement.execute();
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
