package net.therap.dao;

import net.therap.controller.DatabaseController;
import net.therap.model.Trainee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al.imran
 * @since 03/04/2021
 */
public class TraineeDao implements Dao<Trainee> {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String SQL_INSERT = "INSERT INTO trainee (name, email, password) VALUES (?, ?, ?);";
    private static final String SQL_READ = "SELECT * FROM trainee WHERE email=? AND password=?;";

    @Override
    public List<Trainee> find(Object object) {
        Trainee trainee = (Trainee) object;
        List<Trainee> list = new ArrayList<>();
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_READ);
            statement.setString(1, trainee.getEmail());
            statement.setString(2, trainee.getPassword());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Trainee newTrainee = new Trainee();
                newTrainee.setId(resultSet.getInt(ID));
                newTrainee.setName(resultSet.getString(NAME));
                newTrainee.setEmail(resultSet.getString(EMAIL));
                newTrainee.setPassword(resultSet.getString(PASSWORD));
                list.add(newTrainee);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

    @Override
    public boolean save(Trainee trainee) {
        Connection connection = DatabaseController.getConnection();

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            connection.setAutoCommit(false);
            statement.setString(1, trainee.getName());
            statement.setString(2, trainee.getEmail());
            statement.setString(3, trainee.getPassword());
            statement.execute();
            connection.commit();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
