package net.therap.view;

/**
 * @author al.imran
 * @since 01/04/2021
 */
public class MessageView {

    public static void show(String message) {
        System.out.println(message);
    }
}
